#include <Arduino.h>
#include <LiquidCrystal.h>
#include "RGB_LED.h"
#include <HX711_ADC.h>

#define LED_INTENSITY 20
#define BLINK_DELAY 1000

#define TOLERANCE 1



// Create new instance of the RGB_LED library
RGB_LED *led;
long t;


// LiquidCrystal
const int rs = 12, en = 7, d0 = 5, d1 = 6, d2 = 3, d3 = 2;
LiquidCrystal lcd(rs, en, d0, d1, d2, d3);

// Load Cell Amplifier
//HX711 constructor (dout pin, sck pin)
HX711_ADC LoadCell(A0, A1);

int scaleValue = 100;


void setup() {
    // Initialize the LED with pins 9, 10, 11 -- red, green, blue
    led = new RGB_LED(9, 10, 11);
    led->setColor(50, 50, 50);

    lcd.begin(20, 4);
    lcd.print("Calibrating ...");
    lcd.noBlink();

    // Load Cell Amplifier
    LoadCell.begin();
    long stabilisingtime = 2000; // tare preciscion can be improved by adding a few seconds of stabilising time
    LoadCell.start(stabilisingtime);
    LoadCell.setCalFactor(2143.7 * 0.98); // user set calibration factor (float)

}

void loop() {

    //update() should be called at least as often as HX711 sample rate; >10Hz@10SPS, >80Hz@80SPS
    //longer delay in scetch will reduce effective sample rate (be carefull with delay() in loop)
    LoadCell.update();

    //get smoothed value from data set + current calibration factor
    if (millis() > t + 250) {
        float i = LoadCell.getData();
        lcd.clear();
        scaleDisplay(i, "Grams", 6);
        t = millis();
    }

    //receive from serial terminal
    if (Serial.available() > 0) {
        float i;
        char inByte = Serial.read();
        if (inByte == 't') LoadCell.tareNoDelay();
    }

    //check if last tare operation is complete
    if (LoadCell.getTareStatus() == true) {
        lcd.clear();
        lcd.print("Tare complete");
    }
}

void scaleDisplay(float weight, String units, int targetWeight) {
    lcd.clear();

    // Print Weight
    lcd.setCursor(0, 1);
    lcd.print(units);
    lcd.print(": ");
    if(weight < 0) {
        lcd.print("0.00");
    }
    else {
        lcd.print(weight);
    }

    // Print Target
    lcd.setCursor(0, 3);
    lcd.print("Goal: ");
    lcd.print(targetWeight);

    
    if(weight + TOLERANCE < targetWeight) {
        lcd.setCursor(17, 0);
        lcd.print("Add");
        led->setColor(LED_INTENSITY, 0, LED_INTENSITY);
    }
    else if(weight - TOLERANCE > targetWeight) {
        lcd.setCursor(14, 0);
        lcd.print("Remove");
        led->setColor(LED_INTENSITY, 0, 0);
    }
    else if(weight == targetWeight) {
        lcd.setCursor(12, 0);
        lcd.print("Perfect!");
        led->setColor(0, LED_INTENSITY, 0);
    }
    else {
        lcd.setCursor(15, 0);
        lcd.print("Great");
        led->setColor(0, LED_INTENSITY, 0);
    }
    
}
