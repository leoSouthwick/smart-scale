#include "RGB_LED.h"

// Can I use bytes instead of byteegers??

// Default Constructor
RGB_LED::RGB_LED(byte red, byte green, byte blue)
{
    // Save pin references
    _red = red;
    _green = green;
    _blue = blue;

    // Set Mode for color pins
    pinMode(_red, OUTPUT);
    pinMode(_green, OUTPUT);
    pinMode(_blue, OUTPUT);
}

void RGB_LED::setColor(byte redValue, byte greenValue, byte blueValue)
{
    // Write pins
    analogWrite(_red, redValue);
    analogWrite(_green, greenValue);
    analogWrite(_blue, blueValue);
}

void RGB_LED::setColorHex(long hex)
{
    // get byte values for each color
    byte redValue = (hex & 0x0F00) >>  16;
    byte greenValue = (hex & 0x00F0) >> 8;
    byte blueValue = (hex & 0x000F);

    // write pins
    analogWrite(_red, redValue);
    analogWrite(_green, greenValue);
    analogWrite(_blue, blueValue);    
}


