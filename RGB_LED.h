/*
  RGB_LED. Control color on a simple 4-Leg RGB_LED Led
  Created by Leo Southwick, Dec 2018.
*/
#ifndef RGB_LED_h
#define RGB_LED_h

#include <Arduino.h>


class RGB_LED
{
  public:
    RGB_LED(byte red_pin, byte blue_pin, byte green_pin);
    void setColor(byte redValue, byte greenValue, byte blueValue);
    void setColorHex(long hex);

  private:
    byte _red;
    byte _green;
    byte _blue;
};

#endif
